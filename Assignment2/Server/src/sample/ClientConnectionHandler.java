package sample;

import java.io.*;
import java.net.Socket;

// Robert Savaglio, 100591436
// Campbell Hamilton, 100582048

class ClientConnectionHandler implements Runnable {
    private Socket client;
    // Path to our files
    private String path = "src/CloudTextFiles";

    // Constructor
    ClientConnectionHandler(Socket a_client) {
        client = a_client;
    }

    @Override
    public void run() {
        try {
            // Socket Input/output setup
            InputStream inputStream = client.getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream);
            BufferedReader in = new BufferedReader(reader);


            // Determine command
            String line = in.readLine();
            if (line.contains("DIR")) {
                System.out.println("DIR command received.");
                try {
                    // Load directory
                    String msg = "";
                    File dir = new File(path);
                    File[] filesList = dir.listFiles();

                    // Create msg from files in the directory
                    for (File file : filesList) {
                        if (file.isFile()) {
                            msg += (file.getName() + "\n");
                        }
                    }

                    // Output message to client
                    PrintWriter out = new PrintWriter((client.getOutputStream()));
                    out.print(msg);
                    out.flush();
                    System.out.println("Dir command executed");

                    // Cleanup
                    out.close();

                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }

            } else if (line.contains("DOWNLOAD")) {
                System.out.println("Download command received.");
                try {

                    // Parse file name
                    line = line.substring(line.indexOf(' ') + 1);

                    // Open the file
                    FileReader fileReader = new FileReader(path + "/" + line);
                    BufferedReader bufferedReader = new BufferedReader(fileReader);
                    String msg = "";

                    // Read through file and save contents
                    while ((line = bufferedReader.readLine()) != null) {
                        msg += (line + "\n");
                    }

                    System.out.println(msg);

                    // Output file contents to client
                    PrintWriter out = new PrintWriter((client.getOutputStream()));
                    out.print(msg);
                    out.flush();

                    System.out.println("Download command executed");

                    // Cleanup
                    out.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (line.contains("UPLOAD")) {

                System.out.println("Upload command received.");

                // Parse file name
                line = line.substring(line.indexOf(' ') + 1);

                try {
                    // Create file
                    FileWriter writer = new FileWriter(path + "/" + line);

                    // Clears the file if the file already existed
                    writer.write("");

                    // Adds the clients message into the file
                    while ((line = in.readLine()) != null) {
                        writer.append(line + "\n");
                    }
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else {
                System.out.println("Error invalid request");
            }

            System.out.println("Request finished, thread closing");

            // Cleanup
            in.close();
            reader.close();
            client.close();

        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

    }
}
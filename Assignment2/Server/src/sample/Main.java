package sample;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;

public class Main  {


    public static void main(String[] args)
    {
        try
        {
            ServerSocket serverSocket = new ServerSocket(8080);

            // Output all of the address's on this computer
            Enumeration e = NetworkInterface.getNetworkInterfaces();
            while(e.hasMoreElements())
            {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements())
                {
                    InetAddress i = (InetAddress) ee.nextElement();
                    System.out.println(i.getHostAddress());
                }
            }

            // Check for connections
            while(true)
            {
                // This will hang until we get a connection
                Socket connection = serverSocket.accept();
                System.out.println("connection received");

                // Thread socket and deal with the message on the thread
                ClientConnectionHandler handler = new ClientConnectionHandler(connection);
                Thread handlerThread = new Thread(handler);
                handlerThread.start();
            }

        }
        catch (java.io.IOException e)
        {
            e.printStackTrace();
        }
    }
}
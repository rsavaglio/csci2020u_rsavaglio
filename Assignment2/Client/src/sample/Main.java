package sample;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.Scene;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

// Robert Savaglio, 100591436
// Campbell Hamilton, 100582048

// Layout Reference: https://docs.oracle.com/javafx/2/layout/size_align.htm

public class Main extends Application {

    // Lists to hold text file names
    public ObservableList<String> localFiles = FXCollections.observableArrayList ();
    public ObservableList<String> cloudFiles = FXCollections.observableArrayList ();

    // Saves the last select file from each list
    public String selectedEditorFile = new String();
    public String selectedLocalFile = new String();
    public String selectedCloudFile = new String();

    // UI
    ListView<String> lstLocal = new ListView<String>();
    ListView<String> lstCloud = new ListView<String>();
    ListView<String> lstLocalEditor = new ListView<String>();
    TextArea txtEditor = new TextArea();

    public static void main(String[] args) {
        Application.launch(Main.class, args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {


        // Get all file names
        getCloudFiles();
        getLocalFiles();

        // Init Layouts
        TabPane tabs = new TabPane();
        tabs.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        Tab tabFileSharing = new Tab();
        tabFileSharing.setText("File Sharing");
        tabFileSharing.setContent(fileSharingTabLayout());

        Tab tabTextEditor = new Tab();
        tabTextEditor.setText("Text Editor");
        tabTextEditor.setContent(textEditorTabLayout());

        tabs.getTabs().addAll(tabFileSharing, tabTextEditor);

        Scene scene = new Scene(tabs, 480, 440); // Manage scene size
        primaryStage.setTitle("Assignment 2 - Client");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();


        ///// Init Listeners for List Selection //////
        // Reference: https://stackoverflow.com/questions/12459086/how-to-perform-an-action-by-selecting-an-item-from-listview-in-javafx-2

        // Local List
        lstLocal.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                System.out.println("Selected Local File: " + newValue);
                selectedLocalFile = newValue;
            }
        });

        // Cloud List
        lstCloud.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                System.out.println("Selected Cloud File: " + newValue);
                selectedCloudFile = newValue;
            }
        });

        // Local File Editor List
        lstLocalEditor.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                System.out.println("Selected Local File: " + newValue);
                selectedEditorFile = newValue;
                displayText(newValue);
            }
        });
    }

    // Get all the local file names
    private void getLocalFiles(){

        File directory = new File("src/LocalTextFiles");
        File[] contents = directory.listFiles();

        for (File current: contents) {
            localFiles.add(current.getName());
        }
    }

    // Send DIR command to server to get all server file names
    private void getCloudFiles(){

        try{
            // Networking
            Socket socket = new Socket(InetAddress.getLocalHost(), 8080);
            PrintWriter out = new PrintWriter((socket.getOutputStream()));
            InputStream inputStream = socket.getInputStream();

            // Command
            String request = "DIR" + "\n";
            out.write(request);
            out.flush();

            // Input Stream
            InputStreamReader reader = new InputStreamReader(inputStream);
            BufferedReader in = new BufferedReader(reader);
            String line;

            // Clear then add the cloud files to the list
            cloudFiles.clear();
            while((line = in.readLine()) != null){
                cloudFiles.add(line);
            }

            // Close streams and socket
            out.close();
            inputStream.close();
            reader.close();
            in.close();
            socket.close();

            System.out.println("Server directory found.");

        } catch (IOException e){
            e.printStackTrace();
        }

    }

    // Adds the text from the selected file to the text area
    private void displayText(String fileName){
        txtEditor.clear();
        try{
            Scanner fileInput = new Scanner(new File("src/LocalFiles/" + fileName)).useDelimiter("\\n");

            while (fileInput.hasNext()){
                txtEditor.appendText(fileInput.next() + "\n");
            }

            fileInput.close();

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Refresh the lists UI
    private void refreshLists(){
        lstLocal.setItems(localFiles);
        lstLocalEditor.setItems(localFiles);
        lstCloud.setItems(cloudFiles);
    }

    /////////// Layout Functions //////////

    private Pane fileSharingTabLayout() {

        BorderPane border = new BorderPane();
        border.setPadding(new Insets(20, 20, 20, 20));


        Label lblLocal = new Label();
        Label lblCloud = new Label();

        Button btnUpload = new Button("Upload ->");
        btnUpload.setOnAction(this::upload);
        Button btnDownload = new Button("<- Download");
        btnDownload.setOnAction(this::download);

        // Create Column for Local Files
        lblLocal.setText("Local Files");
        lblLocal.setPadding(new Insets(0, 0, 0, 45));
        lstLocal.setItems(localFiles);
        lstLocal.setMaxHeight(Control.USE_PREF_SIZE);
        lstLocal.setPrefWidth(150.0);
        lstLocal.setPrefHeight(300);
        VBox vbLocal = new VBox();
        vbLocal.setSpacing(10);
        vbLocal.getChildren().addAll(lblLocal, lstLocal);

        // Create Column for Cloud Files
        lblCloud.setText("Cloud Files");
        lblCloud.setPadding(new Insets(0, 0, 0, 45));
        lstCloud.setItems(cloudFiles);
        lstCloud.setMaxHeight(Control.USE_PREF_SIZE);
        lstCloud.setPrefWidth(150.0);
        lstCloud.setPrefHeight(300);
        VBox vbCloud = new VBox();
        vbCloud.setSpacing(10);
        vbCloud.getChildren().addAll(lblCloud, lstCloud);

        // Create Column of Buttons
        VBox vbButtons = new VBox();
        vbButtons.setSpacing(10);
        vbButtons.setPadding(new Insets(120, 20, 10, 20));
        vbButtons.getChildren().addAll(btnUpload, btnDownload);
        btnUpload.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        btnDownload.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        // Add to Border Pane
        border.setLeft(vbLocal);
        border.setCenter(vbButtons);
        border.setRight(vbCloud);

        return border;
    }

    private Pane textEditorTabLayout() {

        BorderPane border = new BorderPane();
        border.setPadding(new Insets(20, 20, 20, 20));

        // Create Column for Local Files
        Label lblLocal = new Label();
        lblLocal.setText("Local Files");
        lblLocal.setPadding(new Insets(0, 0, 0, 45));
        lstLocalEditor.setItems(localFiles);
        lstLocalEditor.setMaxHeight(Control.USE_PREF_SIZE);
        lstLocalEditor.setPrefWidth(150.0);
        lstLocalEditor.setPrefHeight(300);
        VBox vbLocal = new VBox();
        vbLocal.setSpacing(10);
        vbLocal.getChildren().addAll(lblLocal, lstLocalEditor);

        // Text Area
        txtEditor.setPrefWidth(270);
        txtEditor.setPrefHeight(302);
        VBox vbText = new VBox();
        vbText.setSpacing(10);
        vbText.setPadding(new Insets(26, 0, 0, 0));
        vbText.getChildren().addAll(txtEditor);

        // Save Button
        Button btnSave = new Button("Save");
        btnSave.setOnAction(this::save);
        btnSave.setPrefWidth(440);
        btnSave.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        // Add to Border Pane
        border.setLeft(vbLocal);
        border.setRight(vbText);
        border.setBottom(btnSave);

        return border;
    }

    //////////// Buttons //////////////

    // Uploads the selected local file to the server
    public void upload(ActionEvent event)
    {
        // Check if a file has been selected
        if(selectedLocalFile.equals("") || selectedLocalFile == null)
        {
            System.out.println("No local file selected!");
            return;
        }

        System.out.print("Uploading file..." + "\n");
        try{
            // Open socket and streams
            Socket socket = new Socket(InetAddress.getLocalHost(), 8080);
            PrintWriter out = new PrintWriter((socket.getOutputStream()));
            InputStream inputStream = socket.getInputStream();

            // Create command and fill message
            String request = "UPLOAD " + selectedLocalFile + "\n";
            Scanner fileInput = new Scanner(new File("src/LocalTextFiles/" + selectedLocalFile)).useDelimiter("\\n");
            while (fileInput.hasNext()){
                request += (fileInput.next() + "\n");
            }
            fileInput.close();

            // Send request
            out.write(request);
            out.flush();

            // Cleanup
            out.close();
            inputStream.close();
            socket.close();

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        System.out.println("Upload Complete!");

        getCloudFiles();
        refreshLists();
    }

    // Downloads the selected file from the server
    public void download(ActionEvent event)
    {

        // Check is a file has been selected
        if(selectedCloudFile.equals("") || selectedCloudFile == null)
        {
            System.out.println("No cloud file selected!");
            return;
        }

        System.out.print("Download started...");
        try{
            // Open socket and stream
            Socket socket = new Socket(InetAddress.getLocalHost(), 8080);
            PrintWriter out = new PrintWriter((socket.getOutputStream()));
            InputStream inputStream = socket.getInputStream();

            // Create and send download command
            String request = "DOWNLOAD " + selectedCloudFile + "\n";
            out.write(request);
            out.flush();

            // Create new file from input
            InputStreamReader reader = new InputStreamReader(inputStream);
            BufferedReader in = new BufferedReader(reader);
            String line;

            File inputFile = new File("src/LocalTextFiles/" + selectedCloudFile);
            PrintWriter fileOut = new PrintWriter(inputFile);

            // Print lines into file
            while((line = in.readLine()) != null){
                fileOut.println(line);
            }
            fileOut.close();

            // Add file to the list
            if (!localFiles.contains(selectedCloudFile)){
                localFiles.add(selectedCloudFile);
            }
            refreshLists();

            // Close socket and streams
            out.close();
            inputStream.close();
            reader.close();
            in.close();
            socket.close();

        } catch (IOException e){
            e.printStackTrace();
        }
        System.out.print("Download complete!");
    }

    // Saves any changes made in the text editor
    public void save(ActionEvent event){

        try {
            File outputFile = new File("src/LocalTestFiles/" + selectedEditorFile);
            PrintWriter fileOut = new PrintWriter(outputFile);
            fileOut.println(txtEditor.getText());
            fileOut.close();
            refreshLists();
        } catch (IOException e){
            e.printStackTrace();
        }

        refreshLists();
    }
}
Created by
Campbell Hamilton - 100582048
Robert Savaglio - 100591436


Download the project from:

https://rsavaglio@bitbucket.org/rsavaglio/csci2020u_rsavaglio.git

**NOTE: I accidentally named the branch "DOWNLOAD THIS ONE FOR A2", I meant to write A1.**


To Run:

Assignment1/out/artifacts/Assignment1/Assignment1.jar

Once you run the jar, a window will pop-up for you to select the 
data folder that contains the test and train folders.
(There is a 'data' folder in the src folder of our project if you need it.)



  Modifications we made to the algorithm to increase Accuracy and Presision
-----------------------------------------------------------------------------
When calculating the 0-1 frequency of a word, if it only exists in Ham or Spam,
we set the value to 1. This is because if we calculate a log of 0 it will give us a NAN value.
This means that using a frequency of zero will cause us to have to discard that set of data when 
calculating the probability. So instead we use the smallest number we can which is 1/#of files. 
The reason we dont just use say 0.000001 is because we wanted to use a value that still existed 
in our range of data and wasn't just arbitrary.
// Robert Savaglio, Campbell Hamilton

package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.*;

public class Main extends Application {

    // Maps for training
    private Map<String, Integer> trainHamFreq;
    private Map<String, Integer> trainSpamFreq;
    private Map<String, Double> trainProbabilities;

    public static String pattern = "^[a-zA-Z]+$";
    int spamFileCount;
    int hamFileCount;

    // Test File Objects
    private List<TestFile> testFiles;

    // Results Variables
    private double numFiles;
    private double numTruePos;
    private double numTrueNeg;
    private double numFalsePos;
    private double precision;
    private double accuracy;

    // UI Window Variables
    private TableView<TestFile> testFileTableView;
    private Label finalAccuracy;
    private Label finalPrecision;


    public static void main(String[] args) { Application.launch(args); }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        //////// UI ////////

        primaryStage.setTitle("Spam Or Ham?!");
        BorderPane layout = new BorderPane();
        layout.setPrefWidth(600);

        //Create the table for the Test Files
        TableColumn<TestFile, String> fileCol = new TableColumn<>("File");
        fileCol.setPrefWidth(300);
        fileCol.setCellValueFactory(new PropertyValueFactory<>("filename"));

        TableColumn<TestFile, String> classCol = new TableColumn<>("Actual Class");
        classCol.setPrefWidth(100);
        classCol.setCellValueFactory(new PropertyValueFactory<>("actualClass"));

        TableColumn<TestFile, Float> spamProbCol = new TableColumn<>("Spam Probability");
        spamProbCol.setPrefWidth(200);
        spamProbCol.setCellValueFactory(new PropertyValueFactory<>("spamProbability"));

        this.testFileTableView = new TableView<>();
        this.testFileTableView.getColumns().add(fileCol);
        this.testFileTableView.getColumns().add(classCol);
        this.testFileTableView.getColumns().add(spamProbCol);

        //Create the bottom
        GridPane bottom = new GridPane();
        bottom.setPadding(new Insets(10));
        bottom.setHgap(10);
        bottom.setVgap(10);

        // Labels for Accuracy and Precision
        bottom.add(new Label("Accuracy: "), 0, 0);
        finalAccuracy = new Label();
        bottom.add(finalAccuracy, 1, 0);

        bottom.add(new Label("Precision: "), 0, 1);
        finalPrecision = new Label();
        bottom.add(finalPrecision, 1, 1);

        // Add the Table and Bottom to the layout
        layout.setCenter(testFileTableView);
        layout.setBottom(bottom);

        // Show the window
        Scene scene = new Scene(layout, 600, 500);
        primaryStage.setScene(scene);
        primaryStage.show();


        /////// Training and Testing ///////

        // Init Variables
        trainHamFreq = new TreeMap<>();
        trainSpamFreq = new TreeMap<>();
        trainProbabilities = new TreeMap<>();
        testFiles = new ArrayList<>();

        // Setup up directory chooser
        DirectoryChooser directoryChooser = new DirectoryChooser();

        // Ask the user to select the data folder
        System.out.println("Please choose the folder that contains the test and train folders.");
        File dataDir = directoryChooser.showDialog(primaryStage);

        try
        {

            //// Train ////
            spamFileCount = processTrainingFile(new File(dataDir, "/train/spam"), trainSpamFreq);
            hamFileCount = processTrainingFile(new File(dataDir, "/train/ham"), trainHamFreq);
            calculateProb();

            //// Test ////
            processTestingFile(new File(dataDir, "/test/spam"), "Spam");
            processTestingFile(new File(dataDir, "/test/ham"), "Ham");

            // Calculate Results and update the table
            calculateResults();

        }
        catch (FileNotFoundException e) {
            System.err.println("Invalid input dir");
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }


    }

    //fills the given map with the word occurrences from the given files
    //returns the number of files that have been read
    public int processTrainingFile(File file, Map<String, Integer> map) throws IOException
    {
        System.out.println("Processing Training File: " + file.getAbsolutePath() + "....");

        // If file is a directory recall this method with each individual file in the directory
        if (file.isDirectory())
        {
            File[] contents = file.listFiles();
            for (File current: contents)
            {
                processTrainingFile(current, map);
            }
            return contents.length;
        }
        // If the file is a file parse through it
        else if (file.exists())
        {
            // Create scanner to parse through file
            Scanner scanner = new Scanner(file);
            scanner.useDelimiter("\\s");//"[\s\.;:\?\!,]");//" \t\n.;,!?-/\\");
            Map<String, Integer> tempMap = new TreeMap<>();


            // Run until there are no more words in the file
            while (scanner.hasNext())
            {
                String word = scanner.next();
                word = word.toLowerCase();

                // Check if the word is an actual word
                if (word.matches(pattern))
                {
                    // If the word isn't in the temp Map add it
                    // We're using this to ensure a word is only incremented once per file
                    if(!tempMap.containsKey(word))
                    {
                        tempMap.put(word,1);

                        // Check map for word
                        if (map.containsKey(word))
                        {
                            // Increment the word count
                            map.put(word, map.get(word) + 1);
                        }
                        else
                        {
                            // If the word doesn't exist add it to the map
                            map.put(word, 1);
                        }
                    }
                }
            }

        }
        return 0;
    }

    // This fills the probability map
    public void calculateProb()
    {
        Set<String> keys = trainSpamFreq.keySet();
        Iterator<String> keyIterator = keys.iterator();
        int spamCount;
        int hamCount;

        // Iterate through the SPAM frequency and calculate probabilities
        while (keyIterator.hasNext())
        {
            String key = keyIterator.next();
            spamCount = trainSpamFreq.get(key);

            // Check if the word exists in HAM frequency and set the count appropriately
            if (trainHamFreq.containsKey(key))
            {
                hamCount = trainHamFreq.get(key);
            }
            else
            {
                hamCount = 1;
            }

            // Calculate the probabilities for the given word to be in a spam email
            double hamProb = (double)hamCount / hamFileCount;
            double spamProb = (double)spamCount / spamFileCount;
            double probability = spamProb / (spamProb + hamProb);

            //save and output the probability
            trainProbabilities.put(key, probability);
            System.out.println(key + " Prob: " + probability);
        }


        keys = trainHamFreq.keySet();
        keyIterator = keys.iterator();

        // Iterate through the ham words to find word that only existed in the ham files
        while (keyIterator.hasNext())
        {
            String key = keyIterator.next();
            // If the word wasn't in a spam file we know the chance for it to be spam is 0
            if (trainProbabilities.containsKey(key) == false)
            {
                double hamProb = (double)trainHamFreq.get(key) / hamFileCount;
                double spamProb = 1.0 / spamFileCount;
                double probability = spamProb / (spamProb + hamProb);

                trainProbabilities.put(key, probability);
                System.out.println(key + " prob: 0");
            }
        }

    }

    void processTestingFile(File file, String hamOrSpam) throws IOException
    {
        System.out.println("Processing Testing File: " + file.getAbsolutePath() + "....");

        // If file is a directory recall this method with each individual file in the directory
        if (file.isDirectory())
        {
            File[] contents = file.listFiles();
            for (File current: contents)
            {
                processTestingFile(current, hamOrSpam);
            }
        }
        // If the file is a file parse through it
        else if (file.exists())
        {

            // Create scanner to parse through file
            Scanner scanner = new Scanner(file);
            scanner.useDelimiter("\\s");//"[\s\.;:\?\!,]");//" \t\n.;,!?-/\\");
            double n = 0.0;

            // For each word do the calculation with the ln formula
            while (scanner.hasNext())
            {
                String word = scanner.next();
                if (trainProbabilities.containsKey(word))
                {
                    double wordProb = trainProbabilities.get(word);

                    //if (wordProb != 0.0 && wordProb != 1.0)
                    //{
                    n += Math.log(1.0 - wordProb) - Math.log(wordProb);
                    //}
                }
            }

            // Now with the eta value for all the word in the file
            // Determine the probability that the file is spam
            double fileSpamProb = 1 / (1 + Math.pow(Math.E, n));


            // Add the results to the list of TestFile Objects
            TestFile result = new TestFile(file.getName(), fileSpamProb, hamOrSpam);
            testFiles.add(result);
        }

    }

    void calculateResults(){


        numFiles = testFiles.size();

        // Value for deciding what probability is spam
        double spamThreshold = 0.5;

        // Check the results for each file
        for (int i = 0; i < numFiles; i++) {

            // File Guessed as Spam
            if (testFiles.get(i).getSpamProbability() >= spamThreshold){

                // Correct Guess
                if (testFiles.get(i).getActualClass() == "Spam")
                {
                    numTruePos++;
                }
                // Incorrect Guess
                else if (testFiles.get(i).getActualClass() == "Ham")
                {
                    numFalsePos++;
                }
            }
            // File Guessed as Ham
            else if (testFiles.get(i).getSpamProbability() < spamThreshold) {

                // Correct Guess
                if (testFiles.get(i).getActualClass() == "Ham") {

                    numTrueNeg++;
                }
            }

            this.testFileTableView.getItems().add(testFiles.get(i));
        }

        // Calculations
        accuracy = (numTruePos + numTrueNeg) / (numFiles);
        precision = (numTruePos) / (numFalsePos + numTruePos);

        finalAccuracy.setText(Double.toString(accuracy));
        finalPrecision.setText(Double.toString(precision));


        System.out.println("Number of Files Tested: " + numFiles);
        System.out.println("True Positives: " + numTruePos);
        System.out.println("True Negatives: " + numTrueNeg);
        System.out.println("False Positive: " + numFalsePos);
        System.out.println("Accuracy: " + accuracy);
        System.out.println("Precision: " + precision);
    }

}


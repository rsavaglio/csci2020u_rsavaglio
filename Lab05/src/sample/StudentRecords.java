package sample;

public class StudentRecords{


    private String studentID;
    private float midterm;
    private float assignments;
    private float finalExam;
    private float finalMark;
    private String letterGrade;


    public StudentRecords(String SID, float ass, float mid, float exam){
        this.studentID = SID;
        this.midterm = mid;
        this.assignments = ass;
        this.finalExam = exam;
        this.finalMark = ((assignments * 0.2f) + (midterm * 0.3f) + (finalExam * 0.5f));

        if (finalMark < 49f){
            this.letterGrade = "F";
        }
        else if (finalMark > 50f && finalMark < 59f){
            this.letterGrade = "D";
        }
        else if (finalMark > 60f && finalMark < 69f){
            this.letterGrade = "C";
        }
        else if (finalMark > 70f && finalMark < 79f){
            this.letterGrade = "B";
        }
        else{
            this.letterGrade = "A";
        }
    }

    public String getStudentID() {return this.studentID;}
    public float getMidterm() {return this.midterm;}
    public float getAssignments() {return this.assignments;}
    public float getFinalExam() {return this.finalExam;}
    public float getFinalMark() {return this.finalMark;}
    public String getLetterGrade() {return this.letterGrade;}

}

package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


import static java.lang.Float.parseFloat;

public class Main extends Application {
    private TableView<StudentRecords> students;
    private TextField sid;
    private TextField ass;
    private TextField mid;
    private TextField exam;

    private Button add;
    private Label addLabel;

    public static void main(String[] args) { Application.launch(args); }


    @Override public void start(Stage primaryStage) {
        primaryStage.setTitle("Lab 05 In Lab Example");

        BorderPane layout = new BorderPane();
        layout.setPrefWidth(600);

        //Create the table of student records
        TableColumn<StudentRecords, String> idCol = new TableColumn<>("SID");
        idCol.setPrefWidth(100);
        idCol.setCellValueFactory(new PropertyValueFactory<>("StudentID"));

        TableColumn<StudentRecords, Float> assCol = new TableColumn<>("Assignments");
        assCol.setPrefWidth(100);
        assCol.setCellValueFactory(new PropertyValueFactory<>("Assignments"));

        TableColumn<StudentRecords, Float> midCol = new TableColumn<>("Midterm");
        midCol.setPrefWidth(100);
        midCol.setCellValueFactory(new PropertyValueFactory<>("Midterm"));

        TableColumn<StudentRecords, Float> examCol = new TableColumn<>("Final Exam");
        examCol.setPrefWidth(100);
        examCol.setCellValueFactory(new PropertyValueFactory<>("FinalExam"));

        TableColumn<StudentRecords, Float> finalCol = new TableColumn<>("Final Mark");
        finalCol.setPrefWidth(100);
        finalCol.setCellValueFactory(new PropertyValueFactory<>("FinalMark"));

        TableColumn<StudentRecords, String> letterCol = new TableColumn<>("Letter Grade");
        letterCol.setPrefWidth(100);
        letterCol.setCellValueFactory(new PropertyValueFactory<>("letterGrade"));

        this.students = new TableView<>();
        this.students.getColumns().add(idCol);
        this.students.getColumns().add(assCol);
        this.students.getColumns().add(midCol);
        this.students.getColumns().add(examCol);
        this.students.getColumns().add(finalCol);
        this.students.getColumns().add(letterCol);

        //Form at the bottom
        this.sid = new TextField();
        this.sid.setPromptText("SID");

        this.ass = new TextField();
        this.ass.setPromptText("Assignment");

        this.mid = new TextField();
        this.mid.setPromptText("Midterm");

        this.exam = new TextField();
        this.exam.setPromptText("Final Exam");

        this.add = new Button("Add");
        this.add.setDefaultButton(true);
        this.add.setOnAction(e -> addStudent());

        this.addLabel = new Label("");

        //Create the form layout
        GridPane bottom = new GridPane();
        bottom.setPadding(new Insets(10));
        bottom.setHgap(10);
        bottom.setVgap(10);

        bottom.add(new Label("SID"), 0, 0);
        bottom.add(sid, 1,0 );

        bottom.add(new Label("Assignment"), 2, 0);
        bottom.add(ass, 3, 0);

        bottom.add(new Label("Midterm"), 0, 1);
        bottom.add(mid, 1, 1);

        bottom.add(new Label("Final Exam"), 2, 1);
        bottom.add(exam, 3, 1);


        bottom.add(add, 1, 3);
        bottom.add(addLabel, 2, 3);


        layout.setCenter(students);
        layout.setBottom(bottom);

        Scene scene = new Scene(layout, 600, 700);
        primaryStage.setScene(scene);
        primaryStage.show();

        this.students.setItems(DataSource.getAllStudents());
    }

    public boolean addStudent() {
        //Check if all fields have values
        String _id = new String(sid.getText());
        Float _ass = parseFloat(ass.getText());
        Float _mid = parseFloat(mid.getText());
        Float _exam = parseFloat(exam.getText());


        if (_id.length() == 0) {
            this.addLabel.setText("Empty Field(s)");
            return false;
        }

        //Add the student

        StudentRecords newStudent = new StudentRecords(_id, _ass, _mid, _exam);

        this.students.getItems().add(newStudent);

        //Clear the fields
        this.sid.setText("");
        this.ass.setText("");
        this.mid.setText("");
        this.exam.setText("");


        return true;
    }
}
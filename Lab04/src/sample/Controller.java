package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import org.apache.commons.validator.routines.EmailValidator;

public class Controller {

    @FXML TextField txtUsername;
    @FXML PasswordField pwPassword;
    @FXML TextField txtFullName;
    @FXML TextField txtEmail;
    @FXML TextField txtPhoneNumber;
    @FXML DatePicker dateDateOfBirth;
    @FXML Label lblInvalidEmail;

    EmailValidator emailValidator;

    @FXML
    public void pressRegister(ActionEvent event){

        System.out.println("Username: "  + txtUsername.getText());
        System.out.println("Password: "  + pwPassword.getText());
        System.out.println("Full Name: "  + txtFullName.getText());
        System.out.println("Email: "  + txtEmail.getText());
        System.out.println("Phone Number: "  + txtPhoneNumber.getText());
        System.out.println("Date of Birth: "  + dateDateOfBirth.getValue());


        emailValidator = EmailValidator.getInstance();
        if (!emailValidator.isValid(txtEmail.getText())) {
            lblInvalidEmail.setText("Invalid E-mail Address");
        }
    }
}

package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Main extends Application {

    private Map<String, Integer> warningsMap;
    int flashFloodTotal;
    int severeThunderTotal;
    int specialMarineTotal;
    int tornadoTotal;
    int totalTotal;
    double flashFloodPercent;
    double severeThunderPercent;
    double specialMarinePercent;
    double tornadoPercent;


    @Override
    public void start(Stage primaryStage) throws Exception{

        warningsMap = new TreeMap<>();

        getData("src/weatherwarnings-2015.csv", ",");

        flashFloodTotal = warningsMap.get("FLASH FLOOD");
        severeThunderTotal = warningsMap.get("SEVERE THUNDERSTORM");
        specialMarineTotal = warningsMap.get("SPECIAL MARINE");
        tornadoTotal = warningsMap.get("TORNADO");

        totalTotal = flashFloodTotal + severeThunderTotal + specialMarineTotal + tornadoTotal;

        //System.out.print(totalTotal);

        flashFloodPercent = (flashFloodTotal / (double)totalTotal) * 360;
        severeThunderPercent = (severeThunderTotal / (double)totalTotal) * 360;
        specialMarinePercent = (specialMarineTotal / (double)totalTotal) * 360;
        tornadoPercent = (tornadoTotal / (double)totalTotal) * 360;




        primaryStage.setTitle("Lab 07");

        Group root = new Group();
        Canvas canvas = new Canvas(550, 300);
        root.getChildren().add(canvas);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.setFill(Color.BLACK);
        gc.fillText("Flash Floods", 100, 70);
        gc.fillText("Severe Thunderstorms", 100, 120);
        gc.fillText("Special Marines", 100, 170);
        gc.fillText("Sharknados", 100, 220);

        gc.setStroke(Color.BLACK);

        gc.setFill(Color.AQUA);
        gc.strokeArc(300,50, 200, 200,0,flashFloodPercent, ArcType.ROUND);
        gc.fillArc(300,50, 200, 200,0, flashFloodPercent, ArcType.ROUND);
        gc.strokeRect(50,50, 40,30);
        gc.fillRect(50,50, 40,30);

        gc.setFill(Color.YELLOW);
        gc.strokeArc(300,50, 200, 200,flashFloodPercent,severeThunderPercent, ArcType.ROUND);
        gc.fillArc(300,50, 200, 200,flashFloodPercent,severeThunderPercent, ArcType.ROUND);
        gc.strokeRect(50,100, 40,30);
        gc.fillRect(50,100, 40,30);

        gc.setFill(Color.ORANGE);
        gc.strokeArc(300,50, 200, 200,flashFloodPercent + severeThunderPercent,specialMarinePercent, ArcType.ROUND);
        gc.fillArc(300,50, 200, 200,flashFloodPercent + severeThunderPercent,specialMarinePercent, ArcType.ROUND);
        gc.strokeRect(50,150, 40,30);
        gc.fillRect(50,150, 40,30);

        gc.setFill(Color.LIGHTBLUE);
        gc.strokeArc(300,50, 200, 200,flashFloodPercent + severeThunderPercent + specialMarinePercent,tornadoPercent, ArcType.ROUND);
        gc.fillArc(300,50, 200, 200,flashFloodPercent + severeThunderPercent + specialMarinePercent,tornadoPercent, ArcType.ROUND);
        gc.strokeRect(50,200, 40,30);
        gc.fillRect(50,200, 40,30);




        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public void getData(String path, String delimiter) {
        List<String> srcList = new ArrayList<>();
        // Special Case
        if (path == null || path.length() == 0) {
            return;
        }
        if(delimiter == null || delimiter.length() == 0) {
            delimiter = ",";
        }

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            //Read the next line until end of file
            for (String line; (line = br.readLine()) != null;) {
                //Parse the line
                String[] values = line.split(delimiter);
                incrementMap(values[5]);

                for(int i = 0; i < values.length; i++) {
                   System.out.print(values[i] + " | ");
                }

                System.out.print("\n");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void incrementMap(String key) {

        // Check map for word
        if (warningsMap.containsKey(key))
        {
            // Increment the word count
            warningsMap.put(key, warningsMap.get(key) + 1);
        }
        else
        {
            // If the word doesn't exist add it to the map
            warningsMap.put(key, 1);
        }
        System.out.print(warningsMap.get(key));
    }


    public static void main(String[] args) throws Exception{
        launch(args);
    }

}